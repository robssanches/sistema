<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return 'Olá mundo!';
});

Route::group(['namespace'=>'Tests'], function () {
    Route::resource('/teste', 'TestController');
});

Route::prefix('admin')->group(function () {

    Route::group(['namespace'=>'Admin'], function () {
        Route::get('/{any?}', 'DashboardController@index')->where('any', '^(?!api\/)[\/\w\.-]*');
    });

});