<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'      => 'Robson Sanches',
            'email'     => 'robssanches@hotmail.com',
            'password'  => bcrypt('123456'),
            'gender'    => 1,
            'birth_date'=> '27/01/1987',
            'tax'       => '999.999.999-99',
            'identity'  => '99.999.999-5',
            'status'    => 1,
            'role_id'   => 1
        ]);

        DB::table('addresses')->insert([
            'number'           => '55',
            'address'          => 'Rua Pedro Nazareno de Menezes Júnior',
            'address_two'      => '',
            'apt'              => '',
            'district'         => 'Ariston III',
            'city'             => 'Carapicuíba',
            'state'            => 'SP',
            'zip_code'         => '06390-630',
            'country'          => 'Brasil',
            'addressable_id'   => $user->id,
            'addressable_type' => 'App\User'
        ]);

        DB::table('contacts')->insert([
            'phone'            => '(11)4202-9478',
            'contact_phone'    => '',
            'cell_phone'       => '(11)94205-3355',
            'contactable_id'   => $user->id,
            'contactable_type' => 'App\User'
        ]);

    }
}
