<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('login')->nullable();
            $table->string('email')->unique();
            $table->integer('gender')->default(0);
            $table->date('birth_date')->nullable();
            $table->string('tax',14)->nullable();
            $table->string('identity',25)->nullable();
            $table->string('password');
            $table->integer('status')->default(0);
            $table->integer('role_id')->default(0);
            $table->integer('media_id')->unsigned()->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
