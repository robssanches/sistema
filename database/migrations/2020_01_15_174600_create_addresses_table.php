<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number',11)->nullable();
            $table->string('address')->nullable();
            $table->string('address_two')->nullable();
            $table->string('apt',10)->nullable();
            $table->string('district')->nullable();
            $table->string('city')->nullable();
            $table->string('state',2)->nullable();
            $table->string('zip_code',9)->nullable();
            $table->string('country')->nullable();
            $table->integer('addressable_id');
            $table->string('addressable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
