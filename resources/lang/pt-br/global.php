<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'version' => 'Versão',
    'dashboard' => 'Painel',
    'search' => 'Pesquisar',
    'logout' => 'Sair',
    'profile' => 'Perfil',
    'copyright' => 'Todos os direitos reservados',
    'home' => 'Início',
    'users' => 'Usuários',
    'sign' => 'Entre para iniciar sua sessão',
    'register' => 'Registrar-se',
    'forgot_pass' => 'Esqueci minha senha',
    'or' => 'ou',
    'sign' => 'Entrar',
    'remember' => 'Lembrar me',
    'sign_facebook' => 'Entrar usando Facebook',
    'sign_google' => 'Entrar usando Google',
    'agree' => 'Eu aceito os',
    'terms' => 'termos',
    'membership' => 'Já tenho cadastro',
    'full_name' => 'Nome Completo',
    'password' => 'Senha',
    'password_confirm' => 'Confirmar Senha',
    'reset_pass' => 'Resetar Senha'

];
