	<!-- Font Awesome -->
	<link href="{{ asset('admin-lte/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<!-- Ionicons -->
	<link href="{{ asset('admin-lte/bower_components/Ionicons/css/ionicons.min.css') }}" rel="stylesheet" />
	<!-- Theme style -->
	<link href="{{ asset('admin-lte/dist/css/AdminLTE.min.css') }}" rel="stylesheet" />
	<!-- Skin -->
	<link href="{{ asset('admin-lte/dist/css/skins/skin-purple.min.css') }}" rel="stylesheet" />
    <!-- iCheck -->
    <link href="{{ asset('admin-lte/plugins/iCheck/square/blue.css') }}" rel="stylesheet" />
	<!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" rel="stylesheet" />   
    <!-- DataTables -->
    <link href="{{ asset('admin-lte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
    <!-- bootstrap datepicker -->
    <link href="{{ asset('admin-lte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
	