{{--
	----------------------

		Admin Template

	----------------------
--}}
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ isset($pageTitle) ? $pageTitle.' | '.config('app.name', 'Admin') : config('app.name', 'Admin') }}</title>

    <!-- Loads Bootstrap -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">

	<!-- Vendor -->
	@include('layouts.admin.css')	

    <!-- CSS (main) -->
	<link href="{{ asset('css/admin.css') }}" rel="stylesheet">	

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js" rel="stylesheet" /></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js" rel="stylesheet" /></script>
	<![endif]-->	

</head>

<body class="hold-transition skin-purple sidebar-mini overlay-wrapper login-page register-page">

	<div id="app">

        <app-layout></app-layout>

	</div>
	<!-- / #app -->

    <!-- Loads jQuery, Bootstrap, VueJS, Lodash and Axios -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Vendor -->
    @include('layouts.admin.js')
	
    <!-- JS (main) -->
	<script src="{{ asset('js/admin.js') }}"></script>

</body>

</html>