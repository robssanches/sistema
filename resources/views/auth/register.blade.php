@extends('layouts.auth')

@section('body_class','register-page')

@section('content')
<div class="register-box">
    <div class="register-logo">
        <a href="{{ url('/') }}">{{ config('app.name') }}</a>
    </div>
    <div class="register-box-body">
        <p class="login-box-msg">
            {{ __('global.register') }}
        </p>

        <form method="POST" action="{{ route('register') }}">

            {{ csrf_field() }}

            <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error' : '' }}">
                <input id="name" type="text" class="form-control" name="name" 
                    value="{{ old('name') }}" placeholder="{{ __('global.full_name') }}" required autofocus>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" 
                        value="{{ old('email') }}" placeholder="E-mail" required>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control" name="password" 
                    placeholder="{{ __('global.password') }}" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback">
                <input id="password-confirm" type="password" class="form-control" 
                    name="password_confirmation" placeholder="{{ __('global.password_confirm') }}" required>
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>
            <div class="row">
                <!--<div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                        <input type="checkbox" name="terms"> {{ __('global.agree') }} <a href="">{{ __('global.terms') }}</a>
                        </label>
                    </div>
                </div>-->
                <!-- /.col -->
                <div class="col-xs-5 col-xs-offset-7">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('global.register') }}</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <!--<div class="social-auth-links text-center">
            <p>
                - {{ __('global.or') }} -
            </p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> {{ __('global.register_facebook') }}</a>
                <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> {{ __('global.register_google') }}</a>
        </div>-->
        <a href="{{ route('login') }}" class="text-center">{{ __('global.membership') }}</a>
    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->
@endsection
