@extends('layouts.auth')

@section('body_class','login-page')

@section('content')
<div class="login-box">

        <div class="login-logo">
            <a href="{{ url('/') }}">{{ config('app.name') }}</a>
        </div>

        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">
                {{ __('global.reset_pass') }}
            </p>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form action="{{ route('password.request') }}" method="POST">

                {{ csrf_field() }}

                <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" 
                        value="{{ old('email') }}" placeholder="E-mail" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="row">
                    <div class="col-xs-6 col-xs-offset-6">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('global.reset_pass') }}</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <a href="{{ route('login') }}">{{ __('global.sign') }}</a><br>
            <a href="{{ route('register') }}" class="text-center">{{ __('global.register') }}</a>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
@endsection
