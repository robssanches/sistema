import crudRouteFactory from './utils/crudRouteFactory.js';

import VueRouter from 'vue-router';

let routes = [
    {
        path: '/',
        name: 'dashboard',
        component: require('./views/admin/home/HomeIndex.vue'),
        meta: {
            title: 'Painel',
            auth: true
        }
    },
    {
        path: '/403',
        name: '403',
        component: require('./views/admin/errors/403.vue'),
        meta: {
            title: 'Erro 403',
            auth: true
        }
    },
    {
        path: '/login',
        name: 'login',
        component: require('./views/auth/Login.vue'),
        meta: {
            title: 'Login',
            auth: false
        }
    },
    crudRouteFactory('/usuarios', 'views/admin/users', 'Users', 'Usuários', {roles:[1], redirect: {name: 'login'}, forbiddenRedirect: '/403'}),
];

export default new VueRouter({
    history: true,
    mode: 'history',
    base: "/admin",
    routes
});