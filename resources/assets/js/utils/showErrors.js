export default function(error){

    let er = error.response.data;

    console.log('error: ',er)

    if('errors' in er){

        let msg = '',
            rows = er.errors;

        for(let e in rows) {
            msg += rows[e][0] + "\n";
        }

        alert(msg);
    
    } else {
        alert(er.message);
    }

}