export default function(routePath, componentPath, filePrefix, pageTitle, auth = false){
    return {
        path: routePath,
        component: require(`../${componentPath}/${filePrefix}Index.vue`),
        meta: {
            title: pageTitle,
            auth
        },
        children: [
            {
                meta: {
                    title: pageTitle,
                    auth
                },
                path: '',
                component: require(`../${componentPath}/${filePrefix}List.vue`)
            },
            {
                meta: {
                    title: pageTitle,
                    auth
                },
                path: 'page/:id?',
                component: require(`../${componentPath}/${filePrefix}List.vue`)
            },
            {
                meta: {
                    title: pageTitle,
                    auth
                },
                path: 'editar/:id',
                component: require(`../${componentPath}/${filePrefix}Edit.vue`)
            },
            {
                meta: {
                    title: pageTitle,
                    auth
                },
                path: 'cadastrar',
                component: require(`../${componentPath}/${filePrefix}Create.vue`)
            }
        ]
    }
}