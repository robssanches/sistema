const loadScripts = {

    data() {
        return {
        }
    },
    mounted() {

        // date picker
        if( $('.datepicker').length > 0 ){
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
        }


        // mask
        //$('.date').inputmask({mask:'dd/mm/yyyy', greedy: false});
        //$('.zip').inputmask({mask:'99999-999', greedy: false});
        //$('.tax').inputmask({mask:'999.999.999-99', greedy: false});
        //$('.phone').inputmask({mask:'(99)9999-9999[9]', greedy: false});

    }
}

export default loadScripts;