import axios from 'axios';

const getUser = {

    data() {
        return {
            user: {
                name: "...",
                email: "..."
            }
        }
    },
    mounted() {

        axios.get('/auth/user')
            .then( resp => {
                this.user = resp.data.data;
                console.log('Current User:',this.user);
            })
            .catch( e => {
                console.log(e.response);
                console.log("Erro ao listar dados do usuário");
            });
    },
    methods: {
        getUser(data){
            return this.user[data];
        }
    }
}

export default getUser;