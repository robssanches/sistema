
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import 'es6-promise/auto'
import axios from 'axios'
import './bootstrap'
import Vue from 'vue'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'

import auth from './auth'
import router from './routes.js';

// Set Vue globally
window.Vue = Vue

// Set Vue router
Vue.router = router
Vue.use(VueRouter)

// Set Vue authentication
Vue.use(VueAxios, axios)
Vue.use(VueAuth, auth)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


// Layout
Vue.component('app-layout', require('./views/Layout.vue'));

// Dashboard
Vue.component('admin-layout', require('./views/admin/dashboard/Layout.vue'));
Vue.component('admin-header', require('./views/admin/dashboard/Header.vue'));
Vue.component('admin-sidebar', require('./views/admin/dashboard/Sidebar.vue'));
Vue.component('admin-menu', require('./views/admin/dashboard/Menu.vue'));
Vue.component('admin-content', require('./views/admin/dashboard/Content.vue'));
Vue.component('admin-footer', require('./views/admin/dashboard/Footer.vue'));
Vue.component('admin-title', require('./views/admin/dashboard/Title.vue'));

// Auth
Vue.component('auth-layout', require('./views/auth/Login.vue'));

// Mixins
// import getCurrentUser from './mixins/getCurrentUser';
import loadScripts from './mixins/loadScripts';

// Vue.mixin(getCurrentUser);
Vue.mixin(loadScripts);

// Components
Vue.component('form-group', require('./components/FormGroup.vue'));
Vue.component('pagination', require('./components/Pagination.vue'));
Vue.component('back-btn', require('./components/BackBtn.vue'));

// Directives
Vue.directive('focus', require('./directives/focus.vue'))

// Instance
const app = new Vue({
    el: '#app',
    router,
    data: {}
});

router.beforeResolve((to, from, next) => {
    //console.log('to',to);
    //console.log('from',from);
    //console.log('app',app);
    next();
});