<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use App\User;
use App\Address;
use App\Contact;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DashboardUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::where('id', '>', 0)->paginate(10);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {

        $user = User::create([
            'name'       => $request->name,
            'email'      => $request->email,
            'password'   => bcrypt($request->password),
            'status'     => $request->status,
            'role_id'    => $request->role_id,
            'gender'     => $request->gender,
            'birth_date' => $request->birth_date,
            'tax'        => $request->tax,
            'identity'   => $request->identity
        ]);

        $address = new Address($request->address);
        $user->address()->save($address);

        $contact = new Contact($request->contact);
        $user->contact()->save($contact);
      
        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        $user->address = $user->address;

        $user->contact = $user->contact;

        return $user;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, $id)
    {

        $user = User::findOrFail($id);

        $user->name       = $request->name;
        $user->email      = $request->email;
        $user->status     = $request->status;
        $user->role_id    = $request->role_id;
        $user->gender     = $request->gender;
        $user->birth_date = $request->birth_date;
        $user->tax        = $request->tax;
        $user->identity   = $request->identity;
        
        if( !empty( $request->password ) ){
            $user->password = bcrypt($request->password);
        }

        $address = $request->address;
        $user->address()->update($address);

        $contact = $request->contact;
        $user->contact()->update($contact);

        $user->save();

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id == 1) return;
        
        $user = User::findOrFail($id);
        
        $user->address()->delete();
        $user->contact()->delete();
        $user->delete();

        return '';
    }

    public function user(Request $request)
    {
        $user = auth()->user();

        return $user ? $user : "Forbidden";
    }

    public function search($s)
    {
        return User::where('name', 'LIKE', '%'.$s.'%')
                ->orWhere('email', 'LIKE', '%'.$s.'%')
                ->paginate(10);
    }
    

}
