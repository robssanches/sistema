<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|string|max:255',
            'email'     => 'required|string|email|max:255',
            'password'  => 'string|min:6',
            'status'    => 'integer|max:1',
            'role_id'   => 'integer|max:11',
            'gender'    => 'integer|max:11',
            'birth_date'=> '',
            'tax'       => 'max:14',
            'identity'  => 'max:25',
            'school_class'  => 'integer|max:255'
        ];
    }


    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'      => 'Nome',
            'email'     => 'E-mail',
            'password'  => 'Senha',
            'status'    => 'Status',
            'role_id'   => 'Tipo de Usuário',
            'gender'    => 'Gênero',
            'birth_date'=> 'Data de Nascimento',
            'tax'       => 'CPF',
            'identity'  => 'Identidade',
            'school_class' => 'Turma'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'name' => 'Campo :attribute é obrigatório!',
        ];
    }
}
