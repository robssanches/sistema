<?php

function getUserRole($id = 0){

    $roles = [
        0 => 'Indefinido',
        1 => 'Admin'
    ];

    return $roles[$id];
}