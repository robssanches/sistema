<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'login',
        'email',
        'gender',
        'birth_date',
        'tax',
        'identity',
        'password',
        'status',
        'role_id',
        'media_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * getJWTIdentifier
     *
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * getJWTCustomClaims
     *
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get the user's address.
     */
    public function address()
    {
        return $this->morphOne(Address::class,'addressable')->withDefault([
            'zip_code' => '',
            'address' => '',
            'address_two' => '',
            'number' => '',
            'apt' => '',
            'district' => '',
            'city' => '',
            'state' => '',
            'country' => ''
        ]);
    }

    /**
     * Get the user's contact.
     */
    public function contact()
    {
        return $this->morphOne(Contact::class,'contactable')->withDefault([
            'phone' => '',
            'contact_phone' => '',
            'cell_phone' => ''
        ]);
    }

    /**
     * Changes date format (Mutator)
     */
    public function setBirthDateAttribute($value)
    {
        if(!$value || $value == '') return;

        $this->attributes['birth_date'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    /**
     * Restore date format (Accessor)
     */
    public function getBirthDateAttribute($value)
    {
        if(!$value || $value == '') return;
        
        return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }

    /**
     * Scope a query to only include admin users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAdmin($query)
    {
        return $query
                ->where('role_id', 1);
    }   


}
