<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone',
        'contact_phone',
        'cell_phone',
        'contactable_id',
        'contactable_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'contactable_id','contactable_type',
    ];

    /**
     * Get the owning addressable model.
     */
    public function contactable()
    {
        return $this->morphTo();
    }
}
